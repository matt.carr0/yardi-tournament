
* Using Open JDK 13 which is the latest I have available
* Using Maven with JUnit, Mockito
* Apache Derby for persistent database
* Database file is stored in the current working directory.
* Using IntellJ for IDE with some code completion features.

* Teams can have only 1 coach, cannot change coach when already set.
* Teams are identified by the name and must be unique.
* Players are identified by firstname, lastname must be unique across all teams.  Players cannot play on 2 teams at once.
* Coaches are identified by firstname, lastname must be unique across all teams.  Coaches cannot coach two teams at once.
* Games can be identified by the home and visitor team.  Assumes each team plays each other at most twice, once as the home team and once as the visitor team.
* Coaches can also be players.
* All string fields are limited to size 63 + \0, there's currently no input checking on length.
