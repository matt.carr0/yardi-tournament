package tournament;

/**
 * Menu selection choices, using an enum instead of magic numbers.
 */
public enum MenuChoiceEnum {
    UNKNOWN, ADD_TEAM, ADD_COACH, ADD_PLAYER, LIST_PLAYERS, LIST_TEAMS,
    CREATE_GAME, GAME_RESULT, DISPLAY_GAMES,
    RESET, EXIT;

    public static MenuChoiceEnum fromInt(int choice) {
        if (choice < 0 || choice >= MenuChoiceEnum.values().length) {
            return MenuChoiceEnum.UNKNOWN;
        }
        return MenuChoiceEnum.values()[choice];
    }

}
