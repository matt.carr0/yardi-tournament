package tournament;

import tournament.data.Coach;
import tournament.data.Game;
import tournament.data.Player;
import tournament.data.Team;
import tournament.controller.Tournament;
import tournament.controller.impl.TournamentImpl;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

import static tournament.MenuChoiceEnum.*;

public class Tournament_main {
    public static void main(String[] args) {

        Tournament_main.showBanner();
        Tournament tournament = new TournamentImpl();
        MenuChoiceEnum input = UNKNOWN;

        try {
            tournament.init();

            while (input != EXIT) {
                showMainMenu();
                input = getValidInput();

                switch (input) {
                    case ADD_COACH: {
                        Team team = tournament.getTeamByName(getTeamName());
                        if (team != null) {
                            tournament.addCoach(team, getCoachInfo());
                        }
                        break;
                    }
                    case ADD_PLAYER: {
                        Team team = tournament.getTeamByName(getTeamName());
                        if (team != null) {
                            tournament.addPlayer(team, getPlayerInfo());
                        }
                        break;
                    }
                    case ADD_TEAM:
                        tournament.addTeam(getTeamName());
                        break;
                    case LIST_PLAYERS:
                        displayPlayers(tournament);
                        break;
                    case LIST_TEAMS:
                        displayTeams(tournament);
                        break;
                    case CREATE_GAME: {
                        Team home = getHomeTeam(tournament);
                        if (home == null) {
                            break;
                        }
                        Team visitor = getVisitorTeam(tournament);
                        if (visitor == null) {
                            break;
                        }
                        tournament.addGame(home, visitor);
                        break;
                    }
                    case GAME_RESULT: {
                        Team home = getHomeTeam(tournament);
                        if (home == null) {
                            break;
                        }
                        Team visitor = getVisitorTeam(tournament);
                        if (visitor == null) {
                            break;
                        }
                        Game game = tournament.getGameByTeams(home, visitor);
                        tournament.setGameResult(game, getScore("home"), getScore("visiting"));
                        break;
                    }
                    case DISPLAY_GAMES:
                        displayGames(tournament);
                        break;
                    case UNKNOWN:
                        System.out.println("Sorry cannot understand reply.  Try again.");
                        break;
                    case RESET:
                        tournament.clear();
                        System.out.println("Data reset");
                        break;
                    case EXIT:
                        System.out.println("Bye!");
                    default:
                        break;
                }
            }
        } finally {
            tournament.close();
        }
    }

    /**
     * Display a fancy banner welcoming people to the program.
     */
    private static void showBanner() {
        System.out.println("Welcome to the tournament!");
    }

    private static void showMainMenu() {
        System.out.println("Choose one of the following actions:");
        System.out.printf(
                """


Team Management:
%d. Add team to the tournament
%d. Add coach to a team.
%d. Add player to a team.
%d. List Players and Coaches for a team.
%d. List Teams.

Game Management:
%d. Create game.
%d. Enter result for game.
%d. Show games and results.

Clear data %d.
Exit %d.
%n""", ADD_TEAM.ordinal(),
        ADD_COACH.ordinal(),
        ADD_PLAYER.ordinal(),
        LIST_PLAYERS.ordinal(),
        LIST_TEAMS.ordinal(),
        CREATE_GAME.ordinal(),
        GAME_RESULT.ordinal(),
        DISPLAY_GAMES.ordinal(),
        RESET.ordinal(),
        EXIT.ordinal());
    }

    private static MenuChoiceEnum getValidInput() {
        try {
            Scanner inputScanner = new Scanner(System.in);
            System.out.print("Enter the menu number: ");
            return fromInt(inputScanner.nextInt());
        } catch (InputMismatchException e) {
            return UNKNOWN;
        }
    }

    private static String getTeamName() {
        System.out.print(" Enter a team name: ");
        return new Scanner(System.in).nextLine();
    }

    private static Team getHomeTeam(Tournament tournament) {
        System.out.print(" Enter the home team name: ");
        String name = new Scanner(System.in).nextLine();
        return tournament.getTeamByName(name);
    }

    private static Team getVisitorTeam(Tournament tournament) {
        System.out.print(" Enter the visiting team name: ");
        String name = new Scanner(System.in).nextLine();
        return tournament.getTeamByName(name);
    }

    private static HashMap<String, String> getParticipantInfo(Scanner scanner) {
        HashMap<String, String> info = new HashMap<>();

        System.out.print(" First Name: ");
        info.put("firstName", scanner.nextLine());
        System.out.print(" Last Name: ");
        info.put("lastName", scanner.nextLine());
        System.out.print(" Contact info: ");
        info.put("contactInfo", scanner.nextLine());
        return info;
    }

    private static Player getPlayerInfo() {
        System.out.println("Please enter the required player info:");
        Scanner scanner = new Scanner(System.in);
        HashMap<String, String> info = getParticipantInfo(scanner);

        System.out.print( " Position: ");
        info.put("position", scanner.nextLine());

        return new Player(info.getOrDefault("firstName", ""),
                info.getOrDefault("lastName", ""),
                info.getOrDefault("contactInfo", ""),
                info.getOrDefault("position", ""));
    }

    private static Coach getCoachInfo() {
        System.out.println("Please enter the required coach info");
        Scanner scanner = new Scanner(System.in);
        HashMap<String, String> info = getParticipantInfo(scanner);

        System.out.print(" Years experience coaching: ");
        return new Coach(info.getOrDefault("firstName", ""),
                info.getOrDefault("lastName", ""),
                info.getOrDefault("contactInfo", ""),
                scanner.nextInt());
    }

    private static int getScore(String type) {
        System.out.printf(" Enter score for %s team: ", type);
        return new Scanner(System.in).nextInt();
    }

    private static void displayPlayers(Tournament tournament) {
        Team team = tournament.getTeamByName(getTeamName());
        if (team != null) {
            System.out.printf("Coach: %s\n", team.getCoach());
            System.out.println("Players:");
            team.getPlayers().forEach(p -> System.out.printf("\t%s\n", p));
        }
    }

    private static void displayTeams(Tournament tournament) {
        tournament.getTeams().forEach(t -> System.out.println(t.getName()));
    }

    private static void displayGames(Tournament tournament) {
        tournament.getGames().forEach(g -> System.out.println(g.toString()));
    }
}
