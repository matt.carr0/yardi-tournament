package tournament.controller;

import tournament.data.Game;
import tournament.data.Team;

/**
 * Main controller acting on the teams, players and games in the tournament.
 */
public interface Tournament extends TournamentDataCommon {

    /**
     * Add a new team to the tournament
     * @param team name to add
     * @return created tournament.data.Team if successful, null otherwise
     */
    Team addTeam(String team);

    /**
     * Add a new game to the tournament, home and visitor team can only play each other once and already be added.
     * @param home existing home team
     * @param visitor existing visitor team
     * @return The created game, or null if already exists
     */
    Game addGame(Team home, Team visitor);

    /**
     * Initialize the data object.
     */
    void init();

    /**
     * close the data object.
     */
    void close();
}
