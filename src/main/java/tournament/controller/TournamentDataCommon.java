package tournament.controller;

import tournament.data.Coach;
import tournament.data.Game;
import tournament.data.Player;
import tournament.data.Team;

import java.util.Collection;

/**d
 * Common functions between the Tournament and TournamentData interfaces.
 */
public interface TournamentDataCommon {
    /**
     * Add a coach to the team
     * @param team to add the coach to
     * @param coach the coach information
     * @return Created coach or null if failure
     */
    Coach addCoach(Team team, Coach coach);

    /**
     * Add a player to the team
     * @param team to add the player to
     * @param player to add
     * @return tournament.data.Player added if successful, null otherwise
     */
    Player addPlayer(Team team, Player player);

    /**
     * Set the final scores for the given game.
     * @param game to set the final scores
     * @param homeScore goals scored by the home team
     * @param visitorScore goals scored by the visiting team.
     * @return if successful, false otherwise
     */
    boolean setGameResult(Game game, int homeScore, int visitorScore);

    /**
     * Get the game given the home and visiting team
     * @param home team
     * @param visitor visiting team
     * @return tournament.data.Game with the home and visiting teams, or null if not found
     */
    Game getGameByTeams(Team home, Team visitor);

    /**
     * Get a team by the name
     * @param name of team
     * @return tournament.data.Team if already created, null otherwise.
     */
    Team getTeamByName(String name);

    /**
     * Return all the games added to the tournament.
     * @return Collection of games, in no particular order
     */
    Collection<Game> getGames();

    /**
     * Return all the teams added to the tournament.
     * @return a collection of teams in no particular order.
     */
    Collection<Team> getTeams();

    /**
     * Clear all the data from the database and local stores.
     */
    void clear();
}
