package tournament.controller.impl;

import org.jetbrains.annotations.VisibleForTesting;
import tournament.data.*;
import tournament.data.impl.GameImpl;
import tournament.data.impl.TeamImpl;
import tournament.data.impl.TournamentDataImpl;
import tournament.controller.Tournament;
import tournament.data.TournamentData;

import java.sql.SQLException;
import java.util.*;

/**
 * Implements the Tournament interface.
 */
public class TournamentImpl implements Tournament {

    /*
     * Games are sets of two unique teams where both can have a score or winner.
     */
    private final TournamentData data;

    public TournamentImpl() {
        data = new TournamentDataImpl();
    }

    @VisibleForTesting
    TournamentImpl(TournamentData data) {
        this.data = data;
    }

    @Override
    public Coach addCoach(Team team, Coach coach) {
        if (team == null || coach == null || !data.containsTeam(team.getName())) {
            System.err.println("Failed to add coach to team, had the team been added?");
            return null;
        }
        return data.addCoach(team, coach);
    }

    @Override
    public Player addPlayer(Team team, Player player) {
        if (team == null || player == null || !data.containsTeam(team.getName())) {
            System.err.println("Failed to add player to team, had the team been added?");
            return null;
        }
        return data.addPlayer(team, player);
    }

    @Override
    public Team addTeam(String teamName) {
        if (data.containsTeam(teamName)) {
            System.err.println("Failed to add team, does a team with the same name already exist?");
            return null;
        }
        return data.addTeam(new TeamImpl(teamName));
    }

    @Override
    public Game addGame(Team home, Team visitor) {
        if (home == null || visitor == null || !data.containsTeam(home.getName()) || !data.containsTeam(visitor.getName()) ||
                home.equals(visitor) || data.containsGame(home.getName(), visitor.getName())) {
            System.err.println("Failed to add game, have the teams been added?");
            return null;
        }
        return data.addGame(new GameImpl(home, visitor));
    }

    @Override
    public void init() {
        //changing exception to something more generic for the frontend.
        try {
            data.init();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            data.close();
        } catch (SQLException e) {
            System.err.printf("Failed to close data object, %s%n", e.getMessage());
        }
    }

    @Override
    public boolean setGameResult(Game game, int homeScore, int visitorScore) {
        if (game == null || !data.containsGame(game.getHomeTeam().getName(), game.getVisitingTeam().getName())) {
            System.err.println("Failed to set game result, has the game been added?");
            return false;
        }
        data.setGameResult(game, homeScore, visitorScore);
        return true;
    }

    @Override
    public Game getGameByTeams(Team home, Team visitor) {
        if (home == null || visitor == null) {
            return null;
        }
        return data.getGameByTeams(home, visitor);

    }

    @Override
    public Team getTeamByName(String name) {
        if (!data.containsTeam(name)) {
            System.err.println("Team does not exist!");
            return null;
        }
        return data.getTeamByName(name);
    }

    @Override
    public Collection<Game> getGames() {
        return data.getGames();
    }

    @Override
    public Collection<Team> getTeams() {
        return data.getTeams();
    }

    @Override
    public void clear() {
        data.clear();
    }
}
