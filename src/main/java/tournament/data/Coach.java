package tournament.data;

import java.util.Objects;

public class Coach extends Participant {

    private final int yearsExperience;

    public Coach(String firstName, String lastName, String contactInfo, int yearsExperience) {
        super(firstName, lastName, contactInfo);
        this.yearsExperience = yearsExperience;
    }

    public Coach(int coach_id, String firstName, String lastName, String contactInfo, int yearsExperience) {
        super(coach_id, firstName, lastName, contactInfo);
        this.yearsExperience = yearsExperience;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Coach coach = (Coach) o;
        return super.equals(coach) && yearsExperience == coach.yearsExperience;
    }

    public int getYearsExperience() {
        return yearsExperience;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), yearsExperience);
    }

    @Override
    public String toString() {
        return super.toString() + ", yearsExperience=" + yearsExperience;
    }
}
