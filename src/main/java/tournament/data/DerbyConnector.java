package tournament.data;

import tournament.controller.TournamentDataCommon;
import tournament.data.impl.TeamImpl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DerbyConnector {

    /**
     * Connect to the database.
     * @throws SQLException when failure
     */
    void connect() throws SQLException;

    /**
     * Close the database.
      * @throws SQLException when failure
     */
    void close() throws SQLException;

    /**
     * Add a team to the database
     * @param team to add
     * @return id for the team
     * @throws SQLException when failure
     */
    int addTeam(Team team) throws SQLException;

    /**
     * add a player to the database
     * @param team to add player to
     * @param player to add
     * @return int of the player's id
     * @throws SQLException when failure
     * @throws IllegalArgumentException when team is missing id
     */
    int addPlayer(Team team, Player player) throws SQLException, IllegalArgumentException;

    /**
     * Add a coach to the database
     * @param team to add the coach to
     * @param coach to add
     * @return int of the coach's id
     * @throws SQLException when failure
     * @throws IllegalArgumentException when team is missing id
     */
    int addCoach(Team team, Coach coach) throws SQLException, IllegalArgumentException;

    /**
     * Add a game to the databse
     * @param game to add
     * @return in of the game id
     * @throws SQLException when failure
     * @throws IllegalArgumentException when team is mising id
     */
    int addGame(Game game) throws SQLException, IllegalArgumentException;

    /**
     * update the score of the game in the database
     * @param game game to update
     * @param home_score goals scored by home team
     * @param visitor_score goals scored by visiting team
     * @throws SQLException when failure
     * @throws IllegalArgumentException when game is missing id
     */
    void updateGameScore(Game game, int home_score, int visitor_score) throws SQLException, IllegalArgumentException;

    /**
     * Get the teams from the database with the coach
     * @return a list of teams and coaches
     * @throws SQLException when failure
     */
    List<TeamImpl> getTeams() throws SQLException;

    /**
     * Get the players for the given team
     * @param team the players are on
     * @return a list of teams
     * @throws SQLException when failure
     */
    List<Player> getPlayers(TeamImpl team) throws SQLException;

    /**
     * Get all games in the database
     * @param teams pre built map of team information to get the team names.
     * @return list of GameImpl
     * @throws SQLException when failure
     */
    List<Game> getGames(Map<String, Team> teams) throws SQLException;

    /**
     * Clear the in memory and persistent data.
     */
    void clear() throws SQLException;
}
