package tournament.data;

/**
 * A tournament.data.Game is a match between two teams with a possible score.
 */
public interface Game {

    /**
     * Add the result of the game
     * @param homeScore score of the home team
     * @param visitorScore score of the visitor team
     */
    void addResult(int homeScore, int visitorScore);

    /**
     * @return tournament.data.Team playing at home
     */
    Team getHomeTeam();

    /**
     * @return the visiting team of the game;
     */
    Team getVisitingTeam();

    /**
     * Set the game's id
     * @param id of the game from the database
     */
    void setId(int id);

}
