package tournament.data;

import java.util.Objects;

/**
 * Particiant is an abstract object representing a person in the tournament.
 */
public abstract class Participant {

    protected final String firstName;
    protected final String lastName;
    protected final String contactInfo;

    protected int participant_id = -1;

    public Participant(String firstName, String lastName, String info) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactInfo = info;
    }

    Participant(int participantId, String firstName, String lastName, String info) {
        this.participant_id = participantId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactInfo = info;
    }

    public void setId(int id) {
        this.participant_id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public int getId() {
        return participant_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participant that = (Participant) o;
        return participant_id == that.participant_id && firstName.equals(that.firstName) && lastName.equals(that.lastName) && contactInfo.equals(that.contactInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(participant_id, firstName, lastName, contactInfo);
    }

    @Override
    public String toString() {
        return "id=" + participant_id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", contactInfo='" + contactInfo + '\'';
    }
}
