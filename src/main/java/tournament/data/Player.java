package tournament.data;

import java.util.Objects;

/**
 * tournament.data.Player object represents a player on a tournament.data.Team. Each player has a jersey number that is unique to the team.
 */
public class Player extends Participant {

    private final String position;

    public Player(String firstName, String lastName, String contactInfo, String position) {
        super(firstName, lastName, contactInfo);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public Player(int playerId, String firstName, String lastName, String contactInfo, String position) {
        super(playerId, firstName, lastName, contactInfo);
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Player player = (Player) o;
        return super.equals(player) && position.equals(player.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), position);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", position='" + position + '\'';
    }
}
