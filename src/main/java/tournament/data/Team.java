package tournament.data;

import java.util.Collection;

/**
 * A team representing a collection of players and a single coach.
 */
public interface Team {


    /**
     * Add a player to the team
     * @param player to add
     * @return false if player's jersey number already exists on the team
     */
    boolean addPlayer(Player player);

    /**
     * Add a coach to the team
     * @param coach for the team
     * @return false if the team already has a coach (no assistant coaches allowed)
     */
    boolean addCoach(Coach coach);

    /**
     *
     * @return Collection of the team's players
     */
    Collection<Player> getPlayers();

    /**
     * Return the Coach for the team.
     * @return
     */
    Coach getCoach();

    /**
     *
     * @return The team's name.
     */
    String getName();

    /**
     * Set the id for the team.
     */
    void setId(int id);

}
