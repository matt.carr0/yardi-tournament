package tournament.data;

import tournament.controller.TournamentDataCommon;

import java.sql.SQLException;

public interface TournamentData extends TournamentDataCommon {

    /**
     * Initialize the database, create the tables if needed.
     * @throws SQLException when connection fails or tables cannot be created.
     */
    void init() throws SQLException;

    /**
     * Close the database
     * @throws SQLException when database cannot be closed
     */
    void close() throws SQLException;

    /**
     * Add a game to the database
     * @param game to add
     * @return the game added or null on failure
     */
    Game addGame(Game game);

    /**
     * Add a team to the database
     * @param team to add
     * @return The team added or null on failure.
     */
    Team addTeam(Team team);

    /**
     * Check if the team exists in the model
     * @param teamName to check
     * @return true if a team in the model matches the team name.
     */
    boolean containsTeam(String teamName);

    /**
     * Check if the game with matching home team and visiting team exists.
     * @param homeTeam name of the home team
     * @param visitingTeam name of the visiting team
     * @return true if game exists matching the home team and visiting team
     */
    boolean containsGame(String homeTeam, String visitingTeam);
}
