package tournament.data.impl;

import org.jetbrains.annotations.VisibleForTesting;
import tournament.data.*;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DerbyConnectorImpl implements DerbyConnector {

    private static final String DB_URL = "jdbc:derby:%s;create=true";
    private static final String DEFAULT_FILENAME = "tournament.db";

    private final File filename;
    private Connection conn;

    public DerbyConnectorImpl() {
        filename = new File(DEFAULT_FILENAME);
    }

    DerbyConnectorImpl(File file) {
        filename = file;
    }

    @Override
    public void connect() throws SQLException {
        boolean doesFileExist = filename.exists();
        connect(!doesFileExist);
    }

    @Override
    public void close() throws SQLException {
        conn.close();
    }

    void connect(boolean createTables) throws SQLException {
        conn = DriverManager.getConnection(String.format(DB_URL, filename));
        if (createTables) {
            createTables();
        }
    }

    @Override
    public int addTeam(Team team) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO teams (team_name) VALUES(?)",
                Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, team.getName());

        return executeAndReturnKey(stmt);
    }

    @Override
    public int addPlayer(Team team, Player player) throws SQLException, IllegalArgumentException {
        PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO players (first_name, last_name, contact_info, position, team_id) VALUES(?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS);
        setParticipantCommon(stmt, player);
        stmt.setString(4, player.getPosition());
        stmt.setInt(5, getTeamId(team));

        return executeAndReturnKey(stmt);
    }

    @Override
    public int addCoach(Team team, Coach coach) throws SQLException, IllegalArgumentException {
        PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO coaches (first_name, last_name, contact_info, years_experience, team_id) VALUES(?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS);
        setParticipantCommon(stmt, coach);
        stmt.setInt(4, coach.getYearsExperience());
        stmt.setInt(5, getTeamId(team));

        return executeAndReturnKey(stmt);

    }

    private void setParticipantCommon(PreparedStatement stmt, Participant participant) throws SQLException {
        stmt.setString(1, participant.getFirstName());
        stmt.setString(2, participant.getLastName());
        stmt.setString(3, participant.getContactInfo());
    }

    private int executeAndReturnKey(PreparedStatement stmt) throws SQLException {
        stmt.executeUpdate();

        ResultSet keys = stmt.getGeneratedKeys();
        keys.next();
        return keys.getInt(1);
    }

    @Override
    public int addGame(Game game) throws SQLException, IllegalArgumentException {
        PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO games(home_team_id, visiting_team_id) VALUES(?, ?)",
                Statement.RETURN_GENERATED_KEYS);
        stmt.setInt(1, getTeamId(game.getHomeTeam()));
        stmt.setInt(2, getTeamId(game.getVisitingTeam()));

        return executeAndReturnKey(stmt);

    }

    @Override
    public void updateGameScore(Game game, int home_score, int visitor_score) throws SQLException, IllegalArgumentException {
        GameImpl gameImpl = (GameImpl)game;
        if (gameImpl == null || gameImpl.game_id == -1) {
            throw new IllegalArgumentException("Game is missing id, was the game added first?");
        }
        PreparedStatement stmt = conn.prepareStatement("Update games set home_score=?, visiting_score=? where game_id=?");

        stmt.setInt(1, home_score);
        stmt.setInt(2, visitor_score);
        stmt.setInt(3, gameImpl.game_id);

        stmt.executeUpdate();
    }

    private int getTeamId(Team team) throws IllegalArgumentException {
        TeamImpl teamImpl = (TeamImpl)team;
        if (teamImpl == null || teamImpl.team_id == -1) {
            throw new IllegalArgumentException("Team is missing id, was the team added first?");
        }
        return teamImpl.team_id;
    }

    @Override
    public List<TeamImpl> getTeams() throws SQLException {
        Statement stmt = conn.createStatement();
        //Using inner join since there is a one to one relation with coaches per team.
        //Doing the same for players requires a bit more logic when determine which team to add them to.
        ResultSet resultSet = stmt.executeQuery("SELECT teams.team_id as team_id, team_name, coach_id, first_name, last_name, contact_info, years_experience " +
                        "FROM teams left join coaches on teams.team_id = coaches.team_id");
        ArrayList<TeamImpl> teams = new ArrayList<>();
        while (resultSet.next()) {
            int coach_id = resultSet.getInt("coach_id");
            if (coach_id == 0) {
                teams.add(new TeamImpl(resultSet.getInt("team_id"), resultSet.getString("team_name")));
            } else {
                teams.add(new TeamImpl(
                        resultSet.getInt("team_id"), resultSet.getString("team_name"),
                        new Coach(
                                coach_id,
                                resultSet.getString("first_name"),
                                resultSet.getString("last_name"),
                                resultSet.getString("contact_info"),
                                resultSet.getInt("years_experience"))));
            }
        }
        return teams;
    }

    @Override
    public List<Player> getPlayers(TeamImpl team) throws SQLException, IllegalArgumentException {
        PreparedStatement stmt = conn.prepareStatement("SELECT player_id, first_name, last_name, contact_info, position FROM players where team_id = ?");
        if (team.team_id == -1) {
            throw new IllegalArgumentException("Team is missing id, check the team is added to the database");
        }
        stmt.setInt(1, team.team_id);
        ResultSet resultSet = stmt.executeQuery();
        ArrayList<Player> players = new ArrayList<>();
        while (resultSet.next()) {
            players.add(new Player(
                    resultSet.getInt("player_id"),
                    resultSet.getString("first_name"),
                    resultSet.getString("last_name"),
                    resultSet.getString("contact_info"),
                    resultSet.getString("position")));
        }
        return players;
    }

    @Override
    public List<Game> getGames(Map<String, Team> teams) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet resultSet = stmt.executeQuery("""
Select game_id, home.team_name as home_team_name, visiting.team_name as visiting_team_name, home_score, visiting_score
FROM games INNER JOIN teams AS home ON games.home_team_id=home.team_id
INNER JOIN teams AS visiting ON games.visiting_team_id=visiting.team_id""");
        ArrayList<Game> games = new ArrayList<>();
        while (resultSet.next()) {
            games.add(new GameImpl(
                    resultSet.getInt("game_id"),
                    teams.get(resultSet.getString("home_team_name")),
                    teams.get(resultSet.getString("visiting_team_name")),
                    resultSet.getInt("home_score"),
                    resultSet.getInt("visiting_score")));
        }
        return games;

    }

    @VisibleForTesting
    void createTables() throws SQLException {
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("""
create table teams (
    team_id integer generated always as identity(start with 1, increment by 1) primary key,
    team_name varchar(64) not null unique
)""");
        stmt.executeUpdate("""
create table players (
    player_id integer generated always as identity(start with 1, increment by 1),
    first_name varchar(64) not null,
    last_name varchar(64) not null,
    contact_info varchar(64),
    position varchar(64),
    team_id integer not null references teams(team_id),
    primary key( first_name, last_name )
)""");
        stmt.executeUpdate("""
create table coaches (
    coach_id integer generated always as identity(start with 1, increment by 1),
    first_name varchar(64) not null,
    last_name varchar(64) not null,
    contact_info varchar(64),
    years_experience integer,
    team_id integer unique not null references teams(team_id),
    primary key( first_name, last_name )
)""");

        stmt.executeUpdate("""
create table games (
    game_id integer not null generated always as identity(start with 1, increment by 1),
    home_team_id integer not null references teams(team_id),
    visiting_team_id integer not null references teams(team_id),
    home_score integer,
    visiting_score integer,
    primary key( home_team_id, visiting_team_id )
 )
                """);
    }

    public void clear() throws SQLException {
        Statement stmt = conn.createStatement();
        stmt.execute("DELETE from players");
        stmt.execute("DELETE from coaches");
        stmt.execute("DELETE from games");
        stmt.execute("DELETE from teams");
    }


}
