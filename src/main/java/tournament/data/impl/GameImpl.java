package tournament.data.impl;

import org.jetbrains.annotations.VisibleForTesting;
import tournament.data.Game;
import tournament.data.Team;

import java.util.Objects;

public class GameImpl implements Game {

    private final Team home;
    private final Team visitor;

    private int scoreHome = -1;
    private int scoreVisitor = -1;

    @VisibleForTesting
    int game_id = -1;

    public GameImpl(Team home, Team visitor) {
        this.home = home;
        this.visitor = visitor;
    }

    GameImpl(int gameId, Team home, Team visitor, int homeScore, int visitorScore) {
        this.game_id = gameId;
        this.home = home;
        this.visitor = visitor;
        this.scoreHome = homeScore;
        this.scoreVisitor = visitorScore;
    }

    @Override
    public void addResult(int homeScore, int visitorScore) {
        scoreHome = homeScore;
        scoreVisitor = visitorScore;
    }

    @Override
    public Team getHomeTeam() {
        return home;
    }

    @Override
    public Team getVisitingTeam() {
        return visitor;
    }

    @Override
    public void setId(int id) {
        game_id=id;
    }

    @VisibleForTesting
    public int getHomeScore() {
        return scoreHome;
    }

    @VisibleForTesting
    public int getVisitorScore() {
        return scoreVisitor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameImpl game = (GameImpl) o;
        return game_id == game.game_id && scoreHome == game.scoreHome && scoreVisitor == game.scoreVisitor && home.equals(game.home) && visitor.equals(game.visitor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(game_id, home, visitor, scoreHome, scoreVisitor);
    }

    @Override
    public String toString() {
        return  "game_id=" + game_id +
                ", home=" + home.getName() +
                ", visitor=" + visitor.getName() +
                ", scoreHome=" + scoreHome +
                ", scoreVisitor=" + scoreVisitor;
    }
}
