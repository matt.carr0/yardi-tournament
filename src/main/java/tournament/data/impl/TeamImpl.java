package tournament.data.impl;

import org.jetbrains.annotations.VisibleForTesting;
import tournament.data.Coach;
import tournament.data.Player;
import tournament.data.Team;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

public class TeamImpl implements Team {

    private final String teamName;
    private Coach coach;
    private final HashSet<Player> players = new HashSet<>();

    @VisibleForTesting
    int team_id = -1;

    public TeamImpl(String teamName) {
        this.teamName = teamName;
    }

    TeamImpl(int team_id, String teamName, Coach coach) {
        this.team_id = team_id;
        this.teamName = teamName;
        this.coach = coach;
    }

    TeamImpl(int team_id, String teamName) {
        this.team_id = team_id;
        this.teamName = teamName;
    }

    @Override
    public boolean addPlayer(Player player) {
        if (players.contains(player)) {
            return false;
        }
        return players.add(player);
    }

    @Override
    public boolean addCoach(Coach newCoach) {
        if (coach == null) {
            this.coach = newCoach;
            return true;
        }
        return false;
    }

    @Override
    public Collection<Player> getPlayers() {
        return players;
    }

    @Override
    public String getName() {
        return teamName;
    }

    @Override
    public void setId(int id) {
        team_id=id;
    }

    @Override
    public Coach getCoach() {
        return coach;
    }

    @Override
    public String toString() {
        return "teamId=" + team_id +
                ", teamName='" + teamName + '\'' +
                ", coach=" + coach +
                ", players=" + players;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamImpl team = (TeamImpl) o;
        return team_id == team.team_id && teamName.equals(team.teamName) && Objects.equals(coach, team.coach) && players.equals(team.players);
    }

    @Override
    public int hashCode() {
        return Objects.hash(team_id, teamName, coach, players);
    }

}
