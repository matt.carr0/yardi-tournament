package tournament.data.impl;

import org.jetbrains.annotations.VisibleForTesting;
import tournament.data.*;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implements the TournamentData interface layer between the tournament class and the database.
 */
public class TournamentDataImpl implements TournamentData {

    private final DerbyConnector connector;

    private final List<Game> games = new ArrayList<>();
    private final Map<String, Team> teams = new HashMap<>();

    public TournamentDataImpl() {
        connector = new DerbyConnectorImpl();
    }

    @VisibleForTesting
    TournamentDataImpl(DerbyConnector connector) {
        this.connector = connector;
    }

    @Override
    public void init() throws SQLException {
        connector.connect();
        teams.putAll(loadTeams());
        games.addAll(loadGames());

    }

    @Override
    public void close() throws SQLException {
        connector.close();
    }

    private Map<String, Team> loadTeams() throws SQLException {
        List<TeamImpl> teams = connector.getTeams();
        for (TeamImpl team: teams) {
            List<Player> players = connector.getPlayers(team);
            players.forEach(team::addPlayer);
        }
        return teams.stream().collect(Collectors.toMap(TeamImpl::getName, t->t));
    }

    private List<Game> loadGames() throws SQLException {
        return connector.getGames(teams);
    }

    @Override
    public Coach addCoach(Team team, Coach coach) {
        try {
            coach.setId(connector.addCoach(team, coach));
        } catch (SQLException | IllegalArgumentException e) {
            System.err.printf("Failed to add coach to team, %s%n", e.getMessage());
            return null;
        }
        team.addCoach(coach);
        return coach;
    }

    @Override
    public Player addPlayer(Team team, Player player) {
        try {
            player.setId(connector.addPlayer(team, player));
        } catch (SQLException | IllegalArgumentException e) {
            System.err.printf("Failed to add player to team %s%n", e.getMessage());
            return null;
        }
        team.addPlayer(player);
        return player;
    }

    @Override
    public Team addTeam(Team team) {
        try {
            team.setId(connector.addTeam(team));
        } catch (SQLException e) {
            System.err.printf("Failed to add team %s%n", e.getMessage());
            return null;
        }
        teams.put(team.getName(), team);
        return team;
    }

    @Override
    public Game addGame(Game game) {
        try {
            game.setId(connector.addGame(game));
        } catch (SQLException | IllegalArgumentException e) {
            System.err.printf("Failed to add game %s%n", e.getMessage());
            return null;
        }
        games.add(game);
        return game;
    }

    @Override
    public boolean setGameResult(Game game, int homeScore, int visitorScore) {
        try {
            connector.updateGameScore(game, homeScore, visitorScore);
        } catch (SQLException | IllegalArgumentException e) {
            System.err.printf("Failed to update score for game, %s%n", e.getMessage());
            return false;
        }
        game.addResult(homeScore, visitorScore);
        return true;
    }

    @Override
    public Game getGameByTeams(Team home, Team visitor) {
        Optional<Game> filtered = games.stream()
                .filter(g -> g.getHomeTeam().equals(home) && g.getVisitingTeam().equals(visitor))
                .findFirst();
        if (filtered.isEmpty()) {
            return null;
        }
        return filtered.get();
    }

    @Override
    public Team getTeamByName(String name) {
        return teams.get(name);
    }

    @Override
    public Collection<Game> getGames() {
        return games;
    }

    @Override
    public Collection<Team> getTeams() {
        return teams.values();
    }

    @Override
    public boolean containsTeam(String teamName) {
        return teams.containsKey(teamName);
    }

    @Override
    public boolean containsGame(String homeTeam, String visitingTeam) {
        return games.stream().anyMatch(
                g -> g.getHomeTeam().getName().equals(homeTeam) && g.getVisitingTeam().getName().equals(visitingTeam));
    }

    @Override
    public void clear() {
        try {
            connector.clear();
        } catch (SQLException e) {
            System.err.printf("Error clearing database, %s%n", e.getMessage());
        }
        games.clear();
        teams.clear();
    }
}
