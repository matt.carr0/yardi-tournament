package tournament.controller.impl;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tournament.data.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tournament.data.impl.GameImpl;
import tournament.data.impl.TeamImpl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TournamentImplTest {

    @Mock
    private TournamentData data;

    private TournamentImpl tournament;
    private Coach coach;
    private Team team1;
    private Team team2;
    private Player player;

    @BeforeEach
    public void setUp() {
        tournament = new TournamentImpl(data);
        coach = new Coach("coach", "lastName", "", 0);
        team1 = new TeamImpl("first team");
        team2 = new TeamImpl("second team");

        player = new Player("player1", "lastname", "123456789", "defence");
    }

    @Test
    void testAddCoachSuccess() {
        when(data.containsTeam(eq(team1.getName()))).thenReturn(true);
        tournament.addCoach(team1, coach);
        verify(data).addCoach(team1, coach);
    }

    @Test
    void testAddCoachFailure() {
        when(data.containsTeam(eq(team1.getName()))).thenReturn(false);
        tournament.addCoach(null, null);
        tournament.addCoach(null, coach);
        tournament.addCoach(team1, coach);
        verify(data, never()).addCoach(any(Team.class), any(Coach.class));
    }

    @Test
    void testAddPlayerSuccess() {
        when(data.containsTeam(eq(team1.getName()))).thenReturn(true);
        tournament.addPlayer(team1, player);
        verify(data).addPlayer(eq(team1), eq(player));
    }

    @Test
    void testAddPlayerFailure() {
        when(data.containsTeam(eq(team1.getName()))).thenReturn(false);
        tournament.addPlayer(null, null);
        tournament.addPlayer(team1, null);
        tournament.addPlayer(team1, player);
        verify(data, never()).addPlayer(any(Team.class), any(Player.class));
    }

    @Test
    void testAddTeamSuccess() {
        when(data.containsTeam(team1.getName())).thenReturn(false);
        tournament.addTeam(team1.getName());
        verify(data).addTeam(any(Team.class));
    }

    @Test
    void testAddTeamFailure() {
        when(data.containsTeam(team1.getName())).thenReturn(true);
        tournament.addTeam(team1.getName());
        verify(data, never()).addTeam(any(Team.class));
    }

    @Test
    void testAddGameSuccess() {
        when(data.containsTeam(anyString())).thenReturn(true);
        when(data.containsGame(anyString(), anyString())).thenReturn(false);
        tournament.addGame(team1, team2);
        tournament.addGame(team2, team1);
        verify(data, times(2)).addGame(any(Game.class));
    }

    @Test
    void testAddGameFailureNullCheck() {
        tournament.addGame(null, null);
        tournament.addGame(team1, null);
        tournament.addGame(null, team2);
        verify(data, never()).addGame(any(Game.class));
    }

    @Test
    void testAddGameFailureTeamNotAdded() {
        when(data.containsTeam(anyString())).thenReturn(false);
        tournament.addGame(team1, team2);
        verify(data, never()).addGame(any(Game.class));
    }

    @Test
    void testAddGameFailureSameTeams() {
        tournament.addGame(team1, team1);
        verify(data, never()).addGame(any(Game.class));
    }

    @Test
    void testAddGameFailureAlreadyExists() {
        tournament.addGame(team1, team2);
        verify(data, never()).addGame(any(Game.class));
    }

    @Test
    void testSetGameResult() {
        GameImpl game = new GameImpl(team1, team2);
        when(data.containsGame(eq(team1.getName()), eq(team2.getName()))).thenReturn(true);
        tournament.setGameResult(game, 2, 1);
        verify(data).setGameResult(game, 2, 1);
    }

    @Test
    void testSetGameResultFail() {
        assertFalse(tournament.setGameResult(null, 0, 0));
        GameImpl game = new GameImpl(team1, team2);
        when(data.containsGame(eq(team1.getName()), eq(team2.getName()))).thenReturn(false);
        assertFalse(tournament.setGameResult(game, 5, 4));
        verify(data, never()).setGameResult(any(Game.class), anyInt(), anyInt());
    }

    @Test
    void testGetGameByTeamsSuccess() {
        tournament.getGameByTeams(team1, team2);
        verify(data).getGameByTeams(any(Team.class), any(Team.class));
    }

    @Test
    void testGetGameByTeamsFail() {
        assertNull(tournament.getGameByTeams(null, null));
        assertNull(tournament.getGameByTeams(team1, null));
        assertNull(tournament.getGameByTeams(null, team2));
        verify(data, never()).getGameByTeams(any(Team.class), any(Team.class));
    }

    @Test
    void testGetTeamByName() {
        when(data.containsTeam(eq(team1.getName()))).thenReturn(true);
        tournament.getTeamByName(team1.getName());
        verify(data).getTeamByName(anyString());
    }

    @Test
    void testGetGames() {
        tournament.getGames();
        verify(data).getGames();
    }

    @Test
    void testGetTeams() {
        tournament.getTeams();
        verify(data).getTeams();
    }
}