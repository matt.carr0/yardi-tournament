package tournament.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoachTest {

    private Coach coach;

    @BeforeEach
    public void setUp() {
        coach = new Coach("coach name", "last name", "555", 4);
    }

    @Test
    public void testCoach() {
        assertTrue(coach instanceof Coach);
        assertTrue(coach instanceof Participant);
    }

    @Test
    public void testEqualsTrue() {
        Coach coach2 = new Coach("coach name", "last name", "555", 4);
        assertEquals(coach, coach2);
        assertEquals(coach.hashCode(), coach2.hashCode());
    }

    @Test
    public void testNotEqualsFalse() {
        Coach coach2 = new Coach("coach name", "last name", "555", 6);
        assertNotEquals(coach, coach2);
        assertNotEquals(coach.hashCode(), coach2.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("id=-1, firstName='coach name', lastName='last name', contactInfo='555', yearsExperience=4",
                coach.toString());
    }

}