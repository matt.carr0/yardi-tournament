package tournament.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    private Player player;

    @BeforeEach
    public void setup() {
        player = new Player("first", "last", "1235557777", "left striker");
    }
    @Test
    public void testPlayer() {
        assertTrue(player instanceof Player);
        assertTrue(player instanceof Participant);
    }

    @Test
    public void testEqualsTrue() {
        Player p2 = new Player("first", "last", "1235557777", "left striker");
        assertEquals(player, p2);
        assertEquals(player.hashCode(), p2.hashCode());
    }

    @Test
    public void testEqualsFalse() {
        Player p2 = new Player("two", "differnt", "456 real", "left defence");
        assertNotEquals(player, p2);
        assertNotEquals(player.hashCode(), p2.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("id=-1, firstName='first', lastName='last', contactInfo='1235557777', position='left striker'", player.toString());
    }

}