package tournament.data.impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tournament.data.Coach;
import tournament.data.Game;
import tournament.data.Player;
import tournament.data.Team;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class DerbyConnectorImplTest {

    private File testDir;
    private DerbyConnectorImpl connector;
    private TeamImpl team;
    private Player player;
    private Coach coach;
    private TeamImpl secondTeam;

    @BeforeEach
    void setUp() throws IOException, SQLException {
        testDir = Files.createTempDirectory("test").toFile();
        connector = new DerbyConnectorImpl(new File(testDir + File.separator + "test.db"));
        connector.connect(false);

        team = new TeamImpl("team");
        secondTeam = new TeamImpl("second team");
        player = new Player("first", "last", "info", "keeper");
        coach = new Coach("first", "last", "info", 20);
    }

    @AfterEach
    void tearDown() {
        testDir.delete();
    }

    void createTables() throws SQLException {
        connector.createTables();
    }

    @Test
    void testCreateTables() {
        assertDoesNotThrow(() -> createTables());
    }

    @Test
    void testAddTeam() throws SQLException {
        createTables();
        int id = connector.addTeam(team);
        assertEquals(1, id);
    }

    @Test
    void testAddTeamSameNameFails() throws SQLException {
        createTables();
        connector.addTeam(team);
        assertThrows(SQLException.class, () -> connector.addTeam(new TeamImpl(team.getName())));
    }

    @Test
    void testAddPlayer() throws SQLException {
        createTables();
        int id = connector.addTeam(team);
        team.team_id = id;
        assertDoesNotThrow(() -> connector.addPlayer(team, player));
    }

    @Test
    void testAddPlayerSameName() throws SQLException, IllegalArgumentException {
        createTables();
        int id = connector.addTeam(team);
        team.team_id = id;
        connector.addPlayer(team, player);
        assertThrows(SQLException.class, () -> connector.addPlayer(team, new Player(player.getFirstName(), player.getLastName(), "other info", "striker")));
    }

    @Test
    void testAddPlayerSameNameDiffTeam() throws SQLException {
        createTables();
        team.team_id = connector.addTeam(team);
        secondTeam.team_id = connector.addTeam(secondTeam);
        connector.addPlayer(team, player);
        assertThrows(SQLException.class, () -> connector.addPlayer(secondTeam, new Player(player.getFirstName(), player.getLastName(), "different", "defence")));
    }

    @Test
    void testAddPlayerTeamNotAdded() throws SQLException {
        createTables();
        assertThrows(IllegalArgumentException.class, ()->connector.addPlayer(team, player));
    }

    @Test
    void testAddCoach() throws SQLException {
        createTables();
        int id = connector.addTeam(team);
        team.team_id = id;
        assertDoesNotThrow( () -> connector.addCoach(team, coach));
    }

    @Test
    void testAddCoachNoTeam() throws SQLException {
        createTables();
        assertThrows(IllegalArgumentException.class, () -> connector.addCoach(team, coach));
    }

    @Test
    void testAddCoachSameName() throws SQLException, IllegalArgumentException {
        createTables();
        int id = connector.addTeam(team);
        team.team_id = id;
        connector.addCoach(team, coach);
        secondTeam.team_id = connector.addTeam(secondTeam);
        assertThrows(SQLException.class, () -> connector.addCoach(secondTeam, new Coach(coach.getFirstName(), coach.getLastName(), "new info", 1)));
    }

    @Test
    void testAddCoachWhenAlreadySet() throws SQLException, IllegalArgumentException {
        createTables();
        int id = connector.addTeam(team);
        team.team_id = id;
        connector.addCoach(team, coach);
        assertThrows(SQLException.class,
                ()->connector.addCoach(team, new Coach("different name", "different last name", "abc", 0)));
    }

    @Test
    void testAddGame() throws SQLException {
        createTables();
        team.team_id = connector.addTeam(team);
        secondTeam.team_id = connector.addTeam(secondTeam);
        assertDoesNotThrow(() -> connector.addGame(new GameImpl(team, secondTeam)));
        assertDoesNotThrow(() -> connector.addGame(new GameImpl(secondTeam, team)));
    }

    @Test
    void testAddGameSameTeams() throws SQLException, IllegalArgumentException {
        createTables();
        team.team_id = connector.addTeam(team);
        secondTeam.team_id = connector.addTeam(secondTeam);
        connector.addGame(new GameImpl(team, secondTeam));
        assertThrows(SQLException.class, ()->connector.addGame(new GameImpl(team, secondTeam)));
    }

    @Test
    void testAddGameTeamsNotAddedFirst() throws SQLException {
        createTables();
        assertThrows(IllegalArgumentException.class, ()->connector.addGame(new GameImpl(team, secondTeam)));
    }

    @Test
    void testUpdateScore() throws SQLException, IllegalArgumentException {
        createTables();
        team.team_id = connector.addTeam(team);
        secondTeam.team_id = connector.addTeam(secondTeam);
        GameImpl game = new GameImpl(team, secondTeam);
        game.game_id = connector.addGame(game);

        assertDoesNotThrow(() -> connector.updateGameScore(game, 5, 1));
    }

    @Test
    void testUpdateScoreGamesNotAdded() throws SQLException {
        createTables();
        team.team_id = connector.addTeam(team);
        secondTeam.team_id = connector.addTeam(secondTeam);
        GameImpl game = new GameImpl(team, secondTeam);
        assertThrows(IllegalArgumentException.class, () -> connector.updateGameScore(game, 1, 1));
    }

    @Test
    void testGetTeams() throws SQLException {
        createTables();
        team.team_id = connector.addTeam(team);
        secondTeam.team_id = connector.addTeam(secondTeam);
        coach.setId(connector.addCoach(team, coach));
        List<Team> expected = new ArrayList<>();
        expected.add(new TeamImpl(1, "team", coach));
        expected.add(new TeamImpl(2, "second team"));
        List<TeamImpl> teams = connector.getTeams();
        assertEquals(2, teams.size());
        assertEquals(expected, connector.getTeams());
    }

    @Test
    void testGetPlayers() throws SQLException {
        createTables();
        team.team_id = connector.addTeam(team);
        ArrayList<Player> expected = new ArrayList<>();
        for (int i=0; i<35; i++) {
            connector.addPlayer(team, new Player("first-%d".formatted(i), "last-%d".formatted(i), "%d".formatted(i), "pos-%d".formatted(i)));
            expected.add(new Player(i+1, "first-%d".formatted(i), "last-%d".formatted(i), "%d".formatted(i), "pos-%d".formatted(i)));
        }

        List<Player> players = connector.getPlayers(team);
        assertEquals(35, players.size());
        assertEquals(players, expected);
    }

    @Test
    void testGetGames() throws SQLException {
        createTables();
        team.team_id = connector.addTeam(team);
        secondTeam.team_id = connector.addTeam(secondTeam);
        coach.setId( connector.addCoach(team, coach));
        team.addCoach(coach);


        GameImpl game1 = new GameImpl(team, secondTeam);
        GameImpl game2 = new GameImpl(secondTeam, team);
        game1.game_id = connector.addGame(game1);
        game2.game_id = connector.addGame(game2);
        connector.updateGameScore(game2, 4, 2);

        List<Game> expected = new ArrayList<>();
        expected.add(new GameImpl(1, team, secondTeam, 0, 0));
        expected.add(new GameImpl(2, secondTeam, team, 4, 2));

        Map<String, Team> teamMap = connector.getTeams().stream().collect(Collectors.toMap(t -> t.getName(), t-> t ));
        List<Game> games = connector.getGames(teamMap);
        assertEquals(2, games.size());
        assertEquals(expected, games);
    }

    @Test
    void testClear() throws SQLException {
        createTables();
        team.team_id = connector.addTeam(team);
        coach.setId( connector.addCoach(team, coach));
        secondTeam.team_id = connector.addTeam(secondTeam);

        GameImpl game1 = new GameImpl(team, secondTeam);
        GameImpl game2 = new GameImpl(secondTeam, team);
        game1.game_id = connector.addGame(game1);
        game2.game_id = connector.addGame(game2);

        for (int i=0; i<35; i++) {
            connector.addPlayer(team, new Player("first-%d".formatted(i), "last-%d".formatted(i), "%d".formatted(i), "pos-%d".formatted(i)));
        }

        connector.clear();

        assertTrue(connector.getTeams().isEmpty());
        assertTrue(connector.getPlayers(team).isEmpty());
        assertTrue(connector.getGames(Collections.emptyMap()).isEmpty());


    }
}