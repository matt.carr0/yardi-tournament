package tournament.data.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tournament.data.Team;
import tournament.data.impl.GameImpl;
import tournament.data.impl.TeamImpl;

import static org.junit.jupiter.api.Assertions.*;

class GameImplTest {

    private GameImpl game;
    private Team team1;
    private Team team2;

    @BeforeEach
    void setUp() {
        team1 = new TeamImpl("first team");
        team2 = new TeamImpl("second team");
        game = new GameImpl(team1, team2);
    }

    @Test
    void testAddResultSuccess() {
        game.addResult(8, 3);
        assertEquals(game.getHomeScore(), 8);
        assertEquals(game.getVisitorScore(), 3);
    }

    @Test
    void testGetHomeTeam() {
        assertEquals(game.getHomeTeam(), team1);
    }

    @Test
    void getVisitingTeam() {
        assertEquals(game.getVisitingTeam(), team2);
    }

    @Test
    void getHomeScore() {
        game.addResult(2, 1);
        assertEquals(game.getHomeScore(), 2);
    }

    @Test
    void getVisitorScore() {
        game.addResult(5, 1);
        assertEquals(game.getVisitorScore(), 1);
    }

    @Test
    void testEquals() {
        GameImpl new_game = new GameImpl(team1, team2);
        assertEquals(game, new_game);
        assertEquals(game.hashCode(), new_game.hashCode());
    }

    @Test
    void testEqualsWithResult() {
        GameImpl new_game = new GameImpl(team1, team2);
        new_game.addResult(9, 2);
        assertNotEquals(game, new_game);
        assertNotEquals(game.hashCode(), new_game.hashCode());
    }

    @Test
    void testToString() {
        assertEquals("game_id=-1, home=first team, visitor=second team, scoreHome=-1, scoreVisitor=-1",
                game.toString());
    }
}