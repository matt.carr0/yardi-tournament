package tournament.data.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tournament.data.Coach;
import tournament.data.Player;
import tournament.data.Team;
import tournament.data.impl.TeamImpl;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class TeamImplTest {

    private TeamImpl team;
    private Player p1;
    private Player p2;
    private Coach coach;

    @BeforeEach
    void setUp() {
        team = new TeamImpl("team_name");
        p1 = new Player("firstName", "lastName", "abc", "");
        p2 = new Player("firstName2", "lastName2", "def", "keeper");
        coach = new Coach("coach", "McCoachersen", "dd", 0);
    }

    @Test
    void testAddPlayerSuccess() {
        assertTrue(team.addPlayer(p1));
        assertTrue(team.addPlayer(p2));
    }

    @Test
    void testGetTeamName() {
        assertEquals(team.getName(), "team_name");
    }

    @Test
    void testAddPlayerExists() {
        team.addPlayer(p1);
        assertFalse(team.addPlayer(p1));
    }

    @Test
    void testAddCoach() {
        assertTrue(team.addCoach(coach));
    }

    @Test
    void testAddCoachFalse() {
        team.addCoach(coach);
        Coach coach2 = new Coach("a", "b", "c", 8);
        assertFalse(team.addCoach(coach2));
    }

    @Test
    void testGetPlayers() {
        team.addPlayer(p1);
        team.addPlayer(p2);
        HashSet<Player> expected = new HashSet<>();
        expected.add(p1);
        expected.add(p2);
        assertEquals(expected, team.getPlayers());
    }

    @Test
    void testEquals() {
        team.addPlayer(p1);
        team.addCoach(coach);
        Team sameTeam = new TeamImpl("team_name");
        sameTeam.addPlayer(p1);
        sameTeam.addCoach(coach);

        assertEquals(team, sameTeam);
        assertEquals(team.hashCode(), sameTeam.hashCode());
    }

    @Test
    void testNotEqualsDiffName() {
        team.addCoach(coach);
        team.addPlayer(p1);
        Team diffName = new TeamImpl("different name");
        diffName.addCoach(coach);
        diffName.addPlayer(p1);
        assertNotEquals(diffName, team);
        assertNotEquals(diffName.hashCode(), team.hashCode());
    }

    @Test
    void testNotEqualDiffCoach() {
        Team diffCoach = new TeamImpl(team.getName());
        diffCoach.addCoach(new Coach("a", "b", "c", 5));
        diffCoach.addPlayer(p1);
        assertNotEquals(diffCoach, team);
        assertNotEquals(diffCoach.hashCode(), team.hashCode());
    }

    @Test
    void testNotEqualsDiffPlayers() {
        Team diffPlayers = new TeamImpl(team.getName());
        diffPlayers.addCoach(coach);
        diffPlayers.addPlayer(p2);
        assertNotEquals(diffPlayers, team);
        assertNotEquals(diffPlayers.hashCode(),  team.hashCode());
    }
}