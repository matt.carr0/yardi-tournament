package tournament.data.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import tournament.data.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class TournamentDataImplTest {

    @Mock
    private DerbyConnector conn;

    private TournamentDataImpl data;
    private Coach coach;
    private TeamImpl team1;
    private TeamImpl team2;
    private Player player1;
    private Player player2;
    private GameImpl game1;
    private GameImpl game2;

    @BeforeEach
    void setUp() {
        data = new TournamentDataImpl(conn);
        team1 = new TeamImpl("team1");
        team2 = new TeamImpl("team2");
        coach = new Coach("coach", "coach", "", 1);
        player1 = new Player("first", "last", "", "");
        player2 = new Player("second", "last", "", "");
        game1 = new GameImpl(team1, team2);
        game2 = new GameImpl(team2, team1);
    }

    private void mockTeams(List<TeamImpl> teams) throws SQLException {
        when(conn.getTeams()).thenReturn(teams);
    }

    private void mockPlayers(TeamImpl team, List<Player> players) throws SQLException {
        when(conn.getPlayers(eq(team))).thenReturn(players);
    }

    private void mockGames(List<Game> games) throws SQLException {
        when(conn.getGames(anyMap())).thenReturn(games);
    }

    @Test
    void testInit() {
        assertDoesNotThrow(() -> data.init());
        assertTrue(data.getGames().isEmpty());
        assertTrue(data.getTeams().isEmpty());
    }

    @Test
    void testInitWithData() throws SQLException {
        team1.setId(1);
        team2.setId(2);
        mockTeams(Arrays.asList(team1, team2));
        mockPlayers(team1, Arrays.asList(player1, player2));
        game2.addResult(2,1);
        mockGames(Arrays.asList(game1, game2));
        data.init();
        assertEquals(Arrays.asList(team1, team2), data.getTeams().stream().toList());
        assertEquals(Arrays.asList(game1, game2), data.getGames());
    }

    @Test
    void testClose() {
        assertDoesNotThrow(() -> data.close());
    }

    @Test
    void testAddCoach() throws SQLException {
        when(conn.addCoach(eq(team1), eq(coach))).thenReturn(1);
        data.addCoach(team1, coach);
        assertEquals(team1.getCoach(), coach);
        assertEquals(1, coach.getId());
    }

    @Test
    void testAddCoachSqlError() throws SQLException {
        when(conn.addCoach(eq(team1), eq(coach))).thenThrow(new SQLException());
        data.addCoach(team1, coach);
        assertNull(team1.getCoach());
        assertEquals(-1, coach.getId());
    }

    @Test
    void testAddPlayer() throws SQLException {
        when(conn.addPlayer(eq(team1), eq(player1))).thenReturn(1);
        data.addPlayer(team1, player1);
        assertEquals(Sets.newSet(player1), team1.getPlayers());
        assertEquals(1, player1.getId());
    }

    @Test
    void testAddPlayerSqlError() throws SQLException {
        when(conn.addPlayer(eq(team1), eq(player1))).thenThrow(new SQLException());
        data.addPlayer(team1, player1);
        assertEquals(Sets.newSet(), team1.getPlayers());
        assertEquals(-1, player1.getId());
    }

    @Test
    void testAddTeam() throws SQLException {
        when(conn.addTeam(eq(team1))).thenReturn(1);
        data.addTeam(team1);
        assertEquals(Arrays.asList(team1), data.getTeams().stream().toList());
        assertEquals(1, team1.team_id);
    }

    @Test
    void testAddTeamFailure() throws SQLException {
        when(conn.addTeam(eq(team1))).thenThrow(new SQLException());
        data.addTeam(team1);
        assertEquals(Arrays.asList(), data.getTeams().stream().toList());
        assertEquals(-1, team1.team_id);
    }

    @Test
    void testAddGame() throws SQLException {
        when(conn.addGame(eq(game1))).thenReturn(1);
        data.addGame(game1);
        assertEquals(Arrays.asList(game1), data.getGames());
        assertEquals(1, game1.game_id);
    }

    @Test
    void testAddGameSqlError() throws SQLException {
        when(conn.addGame(eq(game1))).thenThrow(new SQLException());
        data.addGame(game1);
        assertEquals(Arrays.asList(), data.getGames());
        assertEquals(-1, game1.game_id);
    }

    @Test
    void testSetGameResult() {
        data.setGameResult(game1, 1, 2);
        assertEquals(1, game1.getHomeScore());
        assertEquals(2, game1.getVisitorScore());
    }

    @Test
    void testSetGameResultFailure() throws SQLException {

        doThrow(new SQLException("")).when(conn).updateGameScore(game1, 1, 2);
        data.setGameResult(game1, 1, 2);
        assertEquals(-1, game1.getHomeScore());
        assertEquals(-1, game1.getVisitorScore());
    }

    @Test
    void testGetGameByTeams() {
        data.addGame(game1);
        data.addGame(game2);
        assertEquals(game1, data.getGameByTeams(team1, team2));
        assertEquals(game2, data.getGameByTeams(team2, team1));
    }

    @Test
    void testGetTeamByName() {
        data.addTeam(team1);
        data.addTeam(team2);
        assertEquals(team1, data.getTeamByName(team1.getName()));
    }

    @Test
    void testContainsTeam() {
        data.addTeam(team1);
        assertTrue(data.containsTeam(team1.getName()));
        assertFalse(data.containsTeam(team2.getName()));
    }

    @Test
    void testContainsGame() {
        data.addGame(game1);

        assertTrue(data.containsGame(team1.getName(), team2.getName()));
        assertFalse(data.containsGame(team2.getName(), team1.getName()));
    }
}